package main

import (
	"fmt"
	"webLinux/app"
)

func main() {
	err := app.Ctx().WebEngine.Run(":48080")
	if err != nil {
		fmt.Println(err)
	}
}
