package entity

import (
	"github.com/jinzhu/gorm"
)

const (
	UserTypeAdmin   = "admin"
	UserTypeTeacher = "teacher"
	UserTypeStudent = "student"
)

type User struct {
	gorm.Model

	Username string
	Password string
	UserType string

	FirstName string
	LastName  string
}
