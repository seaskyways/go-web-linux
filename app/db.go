package app

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
	"path/filepath"
	. "webLinux/app/entity"
)

func initDb() *gorm.DB {
	abs, _ := filepath.Abs("./")
	db, err := gorm.Open("sqlite3", abs+"/app.sqlite")
	if err != nil {
		log.Fatalf("Db open failed: %v", err)
	}

	db.AutoMigrate(
		&User{},
	)

	return db
}
