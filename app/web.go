package app

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"webLinux/app/entity"
)

func InitRouter() *gin.Engine {
	engine := gin.Default()
	RegisterEngine(engine)
	return engine
}

func RegisterEngine(eng *gin.Engine) {
	eng.LoadHTMLGlob("templates/**")
	eng.GET("/", pageSignIn)
	eng.POST("/", actionSignIn)

	userGrp := eng.Group("/user")
	userGrp.Use(sessionMiddleware(false))
	userGrp.POST("/sign-in", apiUserSignIn)
}

func pageSignIn(c *gin.Context) {
	c.HTML(200, "index.tmpl", nil)
}

func actionSignIn(c *gin.Context) {
	var in struct {
		Username string `form:"Username" binding:"required"`
		Password string `form:"Password" binding:"required"`
	}
	if err := c.ShouldBind(&in); err != nil {
		msg := err.Error()
		c.HTML(http.StatusUnauthorized, "index.tmpl", gin.H{
			"error": msg,
		})
		return
	}

	var dbUser entity.User
	err := Ctx().Db.First(&dbUser, entity.User{Username: in.Username, Password: in.Password}).Error
	if err != nil {
		c.AbortWithError(http.StatusUnauthorized, err)
		return
	}

	saveSession(dbUser.ID)

}
