package app

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"sync"
)

type Context struct {
	Db        *gorm.DB
	WebEngine *gin.Engine
}

var _ctx *Context
var ctxInitializer = sync.Once{}

func Ctx() *Context {
	ctxInitializer.Do(buildCtx)
	return _ctx
}

func buildCtx() {
	_ctx = new(Context)
	_ctx.Db = initDb()
	_ctx.WebEngine = InitRouter()
}
