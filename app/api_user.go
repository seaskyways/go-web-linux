package app

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"webLinux/app/entity"
)

func apiUserSignIn(c *gin.Context) {
	type Input struct {
		Username string `json:"username" binding:"required"`
		Password string `json:"password" binding:"required"`
	}
	input := Input{}
	if c.Bind(&input) != nil {
		return
	}

	var dbUser entity.User
	err := Ctx().Db.First(&dbUser, entity.User{Username: input.Username, Password: input.Password}).Error
	if err != nil {
		c.AbortWithError(http.StatusUnauthorized, err)
		return
	}

	saveSession(dbUser.ID)
	c.JSON(http.StatusOK, input)
}

func apiGetUsers(c *gin.Context) {

}
